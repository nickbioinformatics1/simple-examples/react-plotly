import { plot } from 'plotly.js/lib/bar';
import React, { Component } from 'react';
import Plot from 'react-plotly.js'

class Timechart extends Component {
    
    constructor(props){
        super(props);
        this.state={data:[]}
    }

    componentDidMount() {
        const endpoint="https://data.cityofnewyork.us/resource/szfr-d24y.json"

        fetch(endpoint) 
            .then(response => response.json())
            .then(data => {
                this.setState({data: data})
            })
    }

    transformData (data) {
        let plot_data = [];
        let x = [];
        let y = [];

        data.map(each => {
            x.push(each.gender)
            y.push(each.case_count)
        })
        plot_data['x'] = x;
        plot_data['y'] = y;
        return(plot_data)
    }

    render() {
        return(
            <div>
                <Plot
                    data = {[
                        {type: 'bar',
                        x: this.transformData(this.state.data)['x'],
                        y: this.transformData(this.state.data)['y']}
                    ]}
                    layout={ {width: 1000, height:500, title: 'Hep B cases in New York'} }
                />
            </div>
        )
    }
}

export default Timechart