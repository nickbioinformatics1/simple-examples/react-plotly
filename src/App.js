import logo from './logo.svg';
import Timechart from './components/timechart';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Timechart/>
      </header>
    </div>
  );
}

export default App;
